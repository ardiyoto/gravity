
// Project: game 
// Created: 2019-04-08

// show all errors
SetErrorMode(2)

// set window properties
SetWindowTitle( "game" )
SetWindowSize( 1024, 768, 0 )
SetWindowAllowResize( 1 ) // allow the user to resize the window

// set display properties
SetVirtualResolution( 1024, 768 ) // doesn't have to match the window
SetOrientationAllowed( 1, 1, 1, 1 ) // allow both portrait and landscape on mobile devices
SetSyncRate( 30, 0 ) // 30fps instead of 60 to save battery
SetScissor( 0,0,0,0 ) // use the maximum available screen space, no black borders
UseNewDefaultFonts( 1 ) // since version 2.0.22 we can use nicer default fonts


iBox = LoadImage("images/objects/Crate.png")
iBall = LoadImage("images/objects/ball.png")

box1 = CreateSprite(iBox)
SetSpritePosition(box1,100,GetVirtualHeight()-GetSpriteHeight(box1))
SetSpritePhysicsOn(box1,1)

box2 = CreateSprite(iBox)
SetSpritePosition(box2,0,GetVirtualHeight()-(GetSpriteHeight(box2)*3))
SetSpritePhysicsOn(box2,1)

ball = CreateSprite(iBall)
SetSpriteSize(ball,100,100)
SetSpritePhysicsOn(ball,2) // Menjadikan gambar jd object
SetSpriteShape(ball,3)
SetSpritePosition(ball,100,0)

ball1 = CreateSprite(iBall)
SetSpriteSize(ball1,100,100)
SetSpritePhysicsOn(ball1,2)
SetSpriteShape(ball1,3)
SetSpritePosition(ball1,100,0)

SetPhysicsGravity(0,100)

do
	//SetPhysicsDebugOn()
	
	if GetRawKeyPressed(13) = 1
		SetSpritePhysicsVelocity(ball,500,-2000)
	elseif GetRawKeyPressed(39) = 1
		SetSpritePhysicsVelocity(ball,GetSpritePhysicsVelocityX(ball)+100,0)
	elseif GetRawKeyPressed(37) = 1
		SetSpritePhysicsVelocity(ball,GetSpritePhysicsVelocityX(ball)-100,0)
	endif
	
    Sync()
loop
